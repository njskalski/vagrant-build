#!/bin/bash

set -x -e

sudo apt update -qq
sudo apt install -y -qq \
	bison \
    build-essential \
    curl \
    dosfstools \
    flex \
    fuse \
    genisoimage \
    git \
    gnupg \
    libfuse-dev \
    nasm \
    parted \
    pkg-config \
    software-properties-common \
    syslinux \
    syslinux-utils \
    texinfo \
    wget


curl https://sh.rustup.rs -sSf | sh -s -- -y 

source $HOME/.cargo/env

echo 'source $HOME/.cargo/env' >> .profile
echo 'export RUST_BACKTRACE=full' >> .profile

# cargo install --version 0.1.1 cargo-config 
# cargo install cargo-xbuild 
# cargo install --version 0.3.20 xargo 

cd
curl -sf https://gitlab.redox-os.org/redox-os/redox/raw/master/bootstrap.sh -o bootstrap.sh

yes | bash -e bootstrap.sh

cd redox
git pull upstream master
git submodule update --recursive --init

make all
make qemu
